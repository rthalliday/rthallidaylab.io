---
layout: page
title: About
description: RTHalliday Stonework and Construction is a small family business specialising in the undertaking and project management of new builds, barn conversions and property renovations.
image: "/images/logo.jpg"
sitemap:
    priority: 0.7
    lastmod: 2017-11-02
    changefreq: weekly
---
## About RT Halliday Stonework

<span class="image left"><img src="{{ "/images/work/landscape-gardening-1.jpg" | absolute_url }}" alt="" /></span>
RTHalliday Stonework and Construction is a small family business specialising in the undertaking and project management of new builds, barn conversions and property renovations.

We are also passionate about producing beautiful stonework that will last for future generations, whether this is stone facing a property, drystone walling, or producing a unique custom stone feature for your garden.

We work closely with our customers at every step to ensure that they get exactly what they want, to the highest specification but at competitive prices.

<span class="image left"><img src="{{ "/images/work/PA100647.jpg" | absolute_url }}" alt="" /></span>

Having built our own family home we appreciate the importance of getting everything ‘just right’ and feeling confident that your needs are being listened to. We enjoy working with you to create special features within the property such as custom-built fire places and stone feature walls to make the space your own.

We undertake all aspects of the building process including plasterboard and plastering, tiling and patios, waterproof tanking, rendering and roofing.

We are based in the beautiful Eden Valley, Cumbria and are happy to travel around Cumbria, North Yorkshire, Lancashire and County Durham for work.

<!-- ### Content is Imortant
<div class="box">
  <p>
  We are based in the beautiful Eden Valley, Cumbria and are happy to travel around Cumbria, North Yorkshire, Lancashire and County Durham for work.
  </p>
</div> -->
