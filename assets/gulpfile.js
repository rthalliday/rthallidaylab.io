var gulp = require('gulp');
var sass = require('gulp-sass');
//var imageop = require('gulp-image-optimization');
var imagemin = require('gulp-imagemin');
 
gulp.task('sass', function () {
  return gulp.src('./sass/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./css'))
    .pipe(gulp.dest('../_site/assets/css'));
});
 
gulp.task('sass:watch', function () {
  gulp.watch('./sass/**/*.scss', ['sass']);
});

// gulp.task('images', function(cb) {
//   gulp.src(['../images/**/*.png','../images/**/*.jpg','../images/**/*.gif','../images/**/*.jpeg']).pipe(imageop({
//       optimizationLevel: 5,
//       progressive: true,
//       interlaced: true
//   })).pipe(gulp.dest('public/images')).on('end', cb).on('error', cb);
// });


gulp.task('images', () =>
	gulp.src('../images/**/*')
		.pipe(imagemin())
		.pipe(gulp.dest('../images-min/'))
);


gulp.task('default', ['sass:watch'], function () {
});